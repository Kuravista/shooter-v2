﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;

public class Missed : MonoBehaviour
{
    public GameManager2 gameManager;
    public GameObject prefabPoint;
    public Canvas parentCanvas;
    private GameObject prefab;
    private Vector3 mousePos;
    private Vector2 movePos;
     
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void MissedClick()
    {
        if (gameManager != null) gameManager.MinPoint();
        RectTransformUtility.ScreenPointToLocalPointInRectangle(
            parentCanvas.transform as RectTransform,
            Input.mousePosition, parentCanvas.worldCamera,
            out movePos);

        mousePos = parentCanvas.transform.TransformPoint(movePos);
        Debug.Log("Minuss " + mousePos);
        prefab = LeanPool.Spawn(prefabPoint, this.transform.parent.transform.parent);
        prefab.transform.localPosition = new Vector3(mousePos.x*80 , mousePos.y*80, this.transform.localPosition.z);
    }
}
