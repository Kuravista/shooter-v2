﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
using System.Linq;
using UnityEngine.SceneManagement;

public class GameManager2 : MonoBehaviour
{
    public GameObject introGame,inGame, endGame;
    public GameObject[] prefabCorona;
    public Transform parent;
    public List<Transform> spawnPos;
    public int point;
    public GameObject spCursor;
    public TFG tFG;

    [Range(10,60)]
    public int limitTime;
    [Range(1f, 5f)]
    public float spawnTime1;
    public GameObject[] countDown;


    [Header("UI ZONE")]
    public TMPro.TextMeshProUGUI tPoint;
    public TMPro.TextMeshProUGUI tTimer,tEndPoint, tHighScore, tError;
    public GameObject uiError;
    private float timer;
    private GameObject _corona1, _corona2;
    private bool startGame = false;
    private float timerSpawn1;
    [SerializeField]
    private GameObject uiMainMenu, uiCountDown;
    private int hScore;

    void OnEnable()
    {
        if (tFG == null) tFG = TFG.tampark;

        tFG.OnInitialize += onInitialize;
        tFG.OnSuccess += onSucces;
        tFG.OnError += onError;
    }

    void OnDisable()
    {
        tFG.OnInitialize -= onInitialize;
        tFG.OnSuccess -= onSucces;
        tFG.OnError -= onError;
    }
    // Start is called before the first frame update
    void Start()
    {
        startGame = false;
        uiMainMenu = introGame.transform.GetChild(3).gameObject;
        uiCountDown = introGame.transform.GetChild(5).gameObject;
        tFG.ldMainLagiButton.SetActive(false);

        tFG.ldMainLagiButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => RetryGame());
        tFG.ldMainLagiButton.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => tFG.HideLeaderboard());

    }

    private void onInitialize(string log)
    {
        Debug.Log("Initialize " + log);
        if (log != "ok")
        {
            uiError.SetActive(true);
            tError.text = log;
        }
    }

    private void onSucces(string log)
    {
        Debug.Log("onSucces " + log);
    }

    private void onError(string log)
    {
        Debug.Log("onError " + log);
        uiError.SetActive(true);
        tError.text = log;

    }

    public void StartGame()
    {
        //init();
        tTimer.gameObject.SetActive(false);
        uiMainMenu.SetActive(false);
        inGame.SetActive(true);
        uiCountDown.SetActive(true);
        startGame = true;
        spCursor.SetActive(true);

        LeanTween.scale(countDown[0], new Vector3(1, 1, 1), .5f).setEase(LeanTweenType.easeOutElastic).setLoopPingPong().setLoopClamp(2);
        LeanTween.scale(countDown[1], new Vector3(1, 1, 1), .5f).setEase(LeanTweenType.easeOutElastic).setLoopPingPong().setLoopClamp(2).setDelay(1);
        LeanTween.scale(countDown[2], new Vector3(1, 1, 1), .5f).setEase(LeanTweenType.easeOutElastic).setLoopPingPong().setLoopClamp(2).setDelay(2);

        LeanTween.moveLocalY(introGame, 1500f, 1f).setEase(LeanTweenType.linear).setDelay(3).setOnComplete(init);
    }

    public void RetryGame()
    {
        timer = 0;
        tPoint.text = "" + point;

        endGame.SetActive(false);
        introGame.transform.localPosition = Vector3.zero;
        uiCountDown.SetActive(false);
        for (int i = 0; i < countDown.Length; i++)
        {
            countDown[i].transform.localScale = Vector3.zero;
        }
        StartGame();

    }


    public void init()
    {
        point = 0;
        timer = 0;
        tPoint.text = "" + point;
        hScore = TFG.tampark.GetHighScore();
        tTimer.gameObject.SetActive(true);


    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (startGame)
        {
            // seconds in float
            timer += Time.deltaTime;
            if ((int)timer < limitTime)
            {
                timerSpawn();
                tTimer.text =""+(limitTime - (int)timer);
            }
            else EndGame();
        }

    }

    private void EndGame()
    {
        spCursor.SetActive(false);
        inGame.SetActive(false);
        endGame.SetActive(true);
        startGame = false;
        if(point > hScore)
        {
            SubmitScore();
            hScore = point;
        }
        tEndPoint.text = "" + point;
        tHighScore.text = "" + hScore;
        tFG.ldMainLagiButton.SetActive(true);
    }

    public void SubmitScore()
    {
        if (point > hScore)TFG.tampark.GotScore(point); //kirim score
    }

    public void AddPoint()
    {
        point = point + 10;
        tPoint.text = "" + point;
    }

    public void MinPoint()
    {
        if(point>0)point = point - 5;
        tPoint.text = "" + point;
    }

    private void timerSpawn()
    {
        timerSpawn1 += Time.deltaTime;

        if (timerSpawn1 > spawnTime1)
        {
            SpawnCorona();
            timerSpawn1 = 0;
        }

    }
    private void SpawnCorona()
    {
        int x = Random.Range(0, spawnPos.Count);
        int y = x + 1;
        
        if (y >= spawnPos.Count) y = x - 1;

        _corona1 = LeanPool.Spawn(prefabCorona[Random.Range(0, prefabCorona.Length)], spawnPos[x]);
        _corona2 = LeanPool.Spawn(prefabCorona[Random.Range(0, prefabCorona.Length)], spawnPos[y]);

        _corona1.transform.localPosition = Vector3.zero;
        _corona1.transform.SetSiblingIndex(0);
        _corona1.GetComponent<Corona2>().gameManager = this;
        _corona2.transform.localPosition = Vector3.zero;
        _corona2.transform.SetSiblingIndex(0);
        _corona2.GetComponent<Corona2>().gameManager = this;

        if (x >= 3)
            _corona1.GetComponent<Corona2>().rightToLeft = true;
        else
            _corona1.GetComponent<Corona2>().rightToLeft = false;

        if (y >= 3)
            _corona2.GetComponent<Corona2>().rightToLeft = true;
        else
            _corona2.GetComponent<Corona2>().rightToLeft = false;

        _corona1.GetComponent<Corona2>().Init();
        _corona2.GetComponent<Corona2>().Init();

        //_corona1.name = "corona_" + spawnPos[x].name;
    }

    public void PindahScene(int x)
    {
        SceneManager.LoadScene(x, LoadSceneMode.Single);
    }

    public void showLeaderboard(bool t)
    {
        TFG.tampark.ShowLeaderboard(t);
    }

    public void BacktoTour()
    {
        TFG.tampark.backToTour();
    }
}
