﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public GameObject[] prefabCorona;
    public Transform parent;
    public List<Transform> spawnPos;
    public List<Transform> pos;
    public List<Transform> list3;

    public float timer;
    public int point;
    [Range(1f, 5f)]
    public float spawnTime1;

    [Header("UI ZONE")]
    public TMPro.TextMeshProUGUI tPoint;

    private int spawnKe=0;
    private float timerSpawn1;
    private bool startGame=false;
    private int seconds;
    private GameObject _corona1;
    [SerializeField]
    private int lastX1 = 0;
    // Start is called before the first frame update
    void Start()
    {
        startGame = true;
        reshuffle(spawnPos);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (startGame)
        {
            // seconds in float
            timer += Time.deltaTime;
            // turn seconds in float to int
            seconds = (int)(timer % 60);
            timerSpawn();

        }
      
    }
    void reshuffle(List<Transform> transform_)
    {
        // Knuth shuffle algorithm :: courtesy of Wikipedia :)
        for (int t = 0; t < transform_.Count; t++)
        {
            Transform tmp = transform_[t];
            int r = Random.Range(t, transform_.Count);
            transform_[t] = transform_[r];
            transform_[r] = tmp;
        }
    }

    public void AddPoint()
    {
        point= point +10;
        tPoint.text = "" + point;
    }

    private void timerSpawn()
    {
        timerSpawn1 += Time.deltaTime;
       

        if (timerSpawn1 > spawnTime1)
        {
            SpawnCorona();
            timerSpawn1 = 0;
        }
     
    }

    private void SpawnCorona()
    {
        //list3 = spawnPos.Except(pos).ToList(); //compare 2 list

        int x = -1;
        if (list3.Count >0)
            x = spawnPos.FindIndex(item => item.name == list3[0].name);

        //Debug.Log("index= " + x);

        if ((pos.Count < spawnPos.Count)&&(x >= 0))
        {
            _corona1 = LeanPool.Spawn(prefabCorona[Random.Range(0,prefabCorona.Length)], spawnPos[x]);
            //_corona1.transform.localPosition = new Vector3(Xpos(x),t.localPosition.y, 0);
            _corona1.transform.localPosition = Vector3.zero;
            _corona1.transform.SetSiblingIndex(0);
            _corona1.GetComponent<Corona>().gameManager = this;
            _corona1.GetComponent<Corona>().index = spawnPos[x].name;
            _corona1.name = "corona_" + spawnPos[x].name;

            pos.Add(spawnPos[x]);
            //spawnKe++;

        }
      
        list3 = spawnPos.Except(pos).ToList(); //compare 2 list

    }

    private int Xpos(int z)
    {
        //jarak 200
        int result;
        int x = Random.Range(-450, 450);
        int y = (z - x);
        if(Mathf.Abs(y) < 150)
        {
            x = Random.Range(-450, 450);
        }
        lastX1 = x;
     
        result = x;

        return result;
    }
}
