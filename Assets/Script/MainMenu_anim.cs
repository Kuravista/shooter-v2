﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu_anim : MonoBehaviour
{
    public GameObject Tittle, btHowto, btLeaderboard, btPlay, btBack;

    // Start is called before the first frame update
    void Start()
    {
        LeanTween.scale(Tittle, new Vector3(1, 1, 1), 1).setEase(LeanTweenType.easeOutElastic);
        LeanTween.scale(btHowto, new Vector3(1, 1, 1), 1f).setEase(LeanTweenType.easeOutElastic).setDelay(.5f);
        LeanTween.scale(btLeaderboard, new Vector3(1, 1, 1), 1f).setEase(LeanTweenType.easeOutElastic).setDelay(1f);
        LeanTween.scale(btPlay, new Vector3(1, 1, 1), 1f).setEase(LeanTweenType.easeOutElastic).setDelay(1.5f);
        LeanTween.scale(btBack, new Vector3(1, 1, 1), 1f).setEase(LeanTweenType.easeOutElastic).setDelay(2f);

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
