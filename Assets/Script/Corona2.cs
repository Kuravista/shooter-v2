﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Pool;

public class Corona2 : MonoBehaviour
{
    public GameManager2 gameManager;
    public bool rightToLeft=false;
    public GameObject prefabPoint;
    public AudioClip[] audioDead;
    public AudioClip[] audioSpawn;

    private Image image;
    private GameObject prefab;
    private AudioSource audioSource;
    private LeanTween tween;
    private float speed=5f;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        audioSource = GetComponent<AudioSource>();
    }
    void OnEnable()
    {
        //Debug.Log("_corona");
        
        LeanTween.scaleY(this.gameObject, 1, 1f).setEase(LeanTweenType.easeOutBack);
        LeanTween.scaleY(this.gameObject, 1.1f, .3f).setEase(LeanTweenType.easeOutBack).setLoopPingPong().setDelay(1f) ;
     

        //LeanTween.scaleY(this.gameObject, 0, .5f).setEase(LeanTweenType.easeOutBack).setDelay(Random.Range(5, 20)).setOnComplete(Dest);
        if (audioSource != null)
            audioSource.PlayOneShot(audioSpawn[Random.Range(0, audioSpawn.Length)]);
    }

    public void Init()
    {
        if (!rightToLeft)
            LeanTween.moveLocalX(this.gameObject, 1500, speed).setEase(LeanTweenType.linear).setDelay(Random.Range(.5f, 3f)).setOnComplete(Dest);
        else
            LeanTween.moveLocalX(this.gameObject, -1500, speed).setEase(LeanTweenType.linear).setDelay(Random.Range(.5f, 3f)).setOnComplete(Dest);
    }

    void OnDisable()
    {
        this.transform.localScale = new Vector3(1, 0, 1);
        this.transform.localPosition = new Vector3(1, 1, 1);
        this.GetComponent<Button>().interactable = true;

        if(image != null )image.color = Color.white;
        speed = Random.Range(2f, 4f);
    }

    public void Dead()
    {
        //Debug.Log("Kill " + this.name);
        this.GetComponent<Button>().interactable = false;
        if (gameManager != null) gameManager.AddPoint();

        image.color = new Color32(25, 25,25,255);
        LeanTween.cancel(this.gameObject);
        LeanTween.scaleY(this.gameObject,0, .5f).setEase(LeanTweenType.linear).setDelay(.5f).setOnComplete(Dest);
        prefab = LeanPool.Spawn(prefabPoint,this.transform.parent);
        prefab.transform.localPosition = new Vector3( this.transform.localPosition.x + Random.Range(-70,70),this.transform.localPosition.y + Random.Range(-70, 70), this.transform.localPosition.z);
        audioSource.PlayOneShot(audioDead[Random.Range(0, audioDead.Length-1)]);
    }
    public void Dest()
    {

        LeanTween.cancel(this.gameObject);
        LeanPool.Despawn(this.gameObject);

    }
}
