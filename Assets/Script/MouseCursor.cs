﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseCursor : MonoBehaviour
{
    private SpriteRenderer sprite;
    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        Cursor.visible = false;
    }

    void OnEnable()
    {
        Cursor.visible = false;

    }

    void OnDisable()
    {
        Cursor.visible = true;

    }

    // Update is called once per frame
    void Update()
    {
        Vector2 cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = cursorPos;
        if (Input.GetMouseButtonDown(0))
        {
            sprite.color = Color.red;
        }else if (Input.GetMouseButtonUp(0))
        {
            sprite.color = Color.white;

        }
    }
}
