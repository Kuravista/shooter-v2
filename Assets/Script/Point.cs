﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Point : MonoBehaviour
{
    // Start is called before the first frame update
    void OnEnable()
    {
        LeanTween.move(this.gameObject, new Vector3(7, 4, 0), Random.Range(.3f,1f)).setEase(LeanTweenType.easeInExpo).setDelay(Random.Range(.5f, 1.5f)).setOnComplete(Dest);
    }

    // Update is called once per frame
    public void Dest()
    {
        Lean.Pool.LeanPool.Despawn(this.gameObject);
    }
}
