﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Lean.Pool;

public class Corona : MonoBehaviour
{
    public GameManager gameManager;
    public string index;
    public GameObject prefabPoint;
    public AudioClip[] audioDead;
    public AudioClip[] audioSpawn;

    private Image image;
    private GameObject prefab;
    private AudioSource audioSource;
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        audioSource = GetComponent<AudioSource>();
    }
    void OnEnable()
    {
        //Debug.Log("_corona");

        LeanTween.scaleY(this.gameObject, 1, 1f).setEase(LeanTweenType.easeOutBack);
        LeanTween.scaleY(this.gameObject, 0, .5f).setEase(LeanTweenType.easeOutBack).setDelay(Random.Range(5, 20)).setOnComplete(Dest);
        if (audioSource != null)
            audioSource.PlayOneShot(audioSpawn[Random.Range(0, audioSpawn.Length-1)]);
    }

    void OnDisable()
    {
        this.transform.localScale = new Vector3(1, 0, 1);
        image.color = Color.white;

    }

    public void Dead()
    {
        //Debug.Log("Kill " + index);
        image.color = new Color32(25, 25,25,255);
        LeanTween.scaleY(this.gameObject,0, .5f).setEase(LeanTweenType.linear).setDelay(.5f).setOnComplete(Dest);
        prefab = LeanPool.Spawn(prefabPoint,this.transform.parent);
        prefab.transform.localPosition = new Vector3( this.transform.localPosition.x + Random.Range(-70,70),this.transform.localPosition.y + Random.Range(-70, 70), this.transform.localPosition.z);
        audioSource.PlayOneShot(audioDead[Random.Range(0, audioDead.Length-1)]);
    }
    public void Dest()
    {
        if(gameManager != null)gameManager.pos.RemoveAll(item => item.name == index);

        LeanTween.cancel(this.gameObject);
        LeanPool.Despawn(this.gameObject);

    }
}
