DAFTAR PERINTAH
---------------
TFG.tampark.GotScore(int jumlahskor); -> Submit skor
TFG.tampark.GetHighScore(); -> Mendapatkan data integer skor tertinggi si user kalau pernah main sebelumnya
TFG.tampark.ShowLeaderboard(true); -> Menampilkan leaderboard dengan tombol "Ayo main lagi"
TFG.tampark.ShowLeaderboard(false); -> Menampilkan leaderboard TANPA tombol "Ayo main lagi"
TFG.tampark.HideLeaderboard(); -> Menyembunyikan leaderboard
TFG.tampark.backToTour(); -> Kembali ke tour

CARA MENGGUNAKAN
----------------
Tambahkan TFGStart pada build settings. Lalu jadikan TFGStart ini scene pertama yang dijalankan (drag ke urutan paling atas). Setelah itu object TFG akan terus running selama game berjalan di scene manapun.

GAME ID
-------
Jangan lupa set Game ID pada object TFG di inspector (di scene TFGStart).

1 = Beat The Corona Virus ???
2 = Find The Difference
3 = Fun Quiz ???
4 = Slot Machine (dulunya Car Racing)
5 = Hidden Words

NEXT SCENE
----------
Jangan lupa masukkan nama scene game yang pertama kali perlu dibuka setelah TFGStart ini berjalan.


PENAMBAHAN POIN DI AKHIR GAME
-----------------------------
Saat game berlangsung pon-poin yang didapat player harus bersifat temporary saja, baru setelah game beakhir, akumulasi poin yang didapat pada sesi permainan silahkan disubmit ke server dengan cara memanggil perintah TFG.tampark.GotScore(int jumlahskor); (seperti contoh-contoh di scene Sample Scene). 

Lakukan pengecekan berapa sih highscore si player ini dengan perintah TFG.tampark.GetHighScore(); lalu bandingkan dengan skor/poin yang didapat sekarang, jika lebih tinggi submit skornya, jika tidak maka tidak usah disubmit.

SAMPLE SCENE
------------
Buka scene Sample Scene, lalu Play

Terlihat teks hijau (info debug, bisa dinonaktifkan pada inspector object TFG (prefab) yang ada di TFG Start scene), di situ akan keluar beberapa info:
- Nama Game
- Nama Pemain
- User ID

Karena saat ini tahap development, maka data user yang muncul di sini adalah data akun dev D001, nanti pada saat event data ini akan secara otomatis tersesuaikan dengan data login si user. 

Coba klik tombol "Dapat Poin 10", akan muncul pesan 10 poin berhasil ditambahkan kepada si user terkait.

Klik tombol Leaderboard. Akan muncul player-player dengan poin tertinggi di game itu. Klik tombol "X" di pojok kanan atas untuk menutup leaderboard.

Untuk men-submit score juga bisa dilakukan dengan perintah TFG.tampark.GotScore(int jumlahskor); seperti yang diimplementasikan pada tombol ketiga "Dapat Poin 123"

Untuk mengetahui berapa highscore si player yang sedang bermain, bisa panggil perintah TFG.tampark.GetHighScore(); lalu kamu akan dapat return integer berisi nilai poin yang ada di server. Contoh implementasinya bisa dilihat di tombol "Berapa highscore saya?". 

Ada contoh Back Button juga di Sample Scene. Perintah "BACK" ini bisa dipanggil begini: TFG.tampark.backToTour();