﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LdbItem : MonoBehaviour
{
	
	public Text rank;
	public Text name;
	public Text point;
	
    public void SetValue(string r, string n, string p){
		rank.text = r;
		name.text = n;
		point.text = p;
	}
	
}
