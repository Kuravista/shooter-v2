﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Clicks : MonoBehaviour
{
	
	public Text highScoreDisplay;
	
	public void gotScore(int score){
		TFG.tampark.GotScore(score);
	}
	
	public void backToTour(){
		TFG.tampark.backToTour();
	}
	
	public void showld(bool showTwoButtons){
		TFG.tampark.ShowLeaderboard(showTwoButtons);
	}
	
	public void hideld(){
		TFG.tampark.HideLeaderboard();
	}
	
	public void dapatSkor123(){
		TFG.tampark.GotScore(123);
	}
	
	public void showHighScore(){
		highScoreDisplay.text = "Highscore: " + TFG.tampark.GetHighScore();
	}
	
	public void reSetUserId(string uid){
		TFG.tampark.reSetUserId(uid);
	}
}
