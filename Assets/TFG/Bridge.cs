﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class Bridge : MonoBehaviour
{

	public void SetBaseUrl(string url){
		TFG.tampark.baseurl = url;
		TFG.tampark.backend = url + "tampan.php";
		TFG.tampark.TfgDebug("Assigning BaseURL: " + url);
	}

    public void SendToUnity(string message)
    {
		TFG.tampark.reSetUserId(message);
		TFG.tampark.currentUserId = message;
    }
	
}
