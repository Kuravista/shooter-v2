﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class TFG : MonoBehaviour
{

	public static TFG tampark;
	public int gameId = 1;	
	public string nextScene = "";
	public bool onScreenDebug = true;
	
	string someUsers;
	
	public GameObject debuginfo;
	public GameObject leaderboard;
	public ScrollRect scrollView;
	public GameObject leaderboardContent;
	public GameObject leaderboardItem;
	
	public Sprite leaderboardBg1;
	public Sprite leaderboardTitle1;
	public Sprite leaderboardCloseButton1;
	public Sprite leaderboardMainLagiButton1;
	
	public Sprite leaderboardBg2;
	public Sprite leaderboardTitle2;
	public Sprite leaderboardCloseButton2;
	public Sprite leaderboardMainLagiButton2;
	
	public Sprite leaderboardBg3;
	public Sprite leaderboardTitle3;
	public Sprite leaderboardCloseButton3;
	public Sprite leaderboardMainLagiButton3;
	
	public Sprite leaderboardBg4;
	public Sprite leaderboardTitle4;
	public Sprite leaderboardCloseButton4;
	public Sprite leaderboardMainLagiButton4;
	
	public Sprite leaderboardBg5;
	public Sprite leaderboardTitle5;
	public Sprite leaderboardCloseButton5;
	public Sprite leaderboardMainLagiButton5;
	
	public GameObject ldBg;
	public GameObject ldTitle;
	public GameObject ldCloseButton;
	public GameObject ldMainLagiButton;
	public GameObject buttonSet0;
	public GameObject buttonSet1;
	
	[HideInInspector]
	public string currentUserId = "";
	[HideInInspector]
	public string currentUserName = "";
	[HideInInspector]
	public string baseurl = "https://dev.sharpauviluck.id/";
	[HideInInspector]
	public string backend = "https://dev.sharpauviluck.id/tampan.php";
	[HideInInspector]
	public int userHighScore = 0;
	
	public delegate void EventPost(string log);
	public EventPost OnError;
	public EventPost OnSuccess;
	public EventPost OnInitialize;
	
	bool externallyInjected = false;
	
    // Start is called before the first frame update
    void Start()
    {
		
		DontDestroyOnLoad(this);
		
		tampark = this;
	
        someUsers = "D0" + Random.Range(0,9) + "" + Random.Range(0,9);
		Debug.Log("Random Dev User ID: " + someUsers);
		TfgDebug("");
		
		if(currentUserId == ""){
			currentUserId = someUsers;
			TfgDebug("Menggunakan User ID percobaan = " + currentUserId + "\nSedang melakukan pengecekan dengan server...");
		}
		
		StartCoroutine(initialCheckUp(gameId, currentUserId));
		
		
		if(!onScreenDebug){
			debuginfo.SetActive(false);
		}
		
		switch(gameId){
			case 1 :
				ldBg.GetComponent<Image>().sprite = leaderboardBg1;
				ldTitle.GetComponent<Image>().sprite = leaderboardTitle1;
				ldCloseButton.GetComponent<Image>().sprite = leaderboardCloseButton1;
				ldMainLagiButton.GetComponent<Image>().sprite = leaderboardMainLagiButton1;
				break;
			case 2 :
				ldBg.GetComponent<Image>().sprite = leaderboardBg2;
				ldTitle.GetComponent<Image>().sprite = leaderboardTitle2;
				ldCloseButton.GetComponent<Image>().sprite = leaderboardCloseButton2;
				ldMainLagiButton.GetComponent<Image>().sprite = leaderboardMainLagiButton2;
				break;
			case 3 :
				ldBg.GetComponent<Image>().sprite = leaderboardBg3;
				ldTitle.GetComponent<Image>().sprite = leaderboardTitle3;
				ldCloseButton.GetComponent<Image>().sprite = leaderboardCloseButton3;
				ldMainLagiButton.GetComponent<Image>().sprite = leaderboardMainLagiButton3;
				break;
			case 4 :
				ldBg.GetComponent<Image>().sprite = leaderboardBg4;
				ldTitle.GetComponent<Image>().sprite = leaderboardTitle4;
				ldCloseButton.GetComponent<Image>().sprite = leaderboardCloseButton4;
				ldMainLagiButton.GetComponent<Image>().sprite = leaderboardMainLagiButton4;
				break;
			case 5 :
				ldBg.GetComponent<Image>().sprite = leaderboardBg5;
				ldTitle.GetComponent<Image>().sprite = leaderboardTitle5;
				ldCloseButton.GetComponent<Image>().sprite = leaderboardCloseButton5;
				ldMainLagiButton.GetComponent<Image>().sprite = leaderboardMainLagiButton5;
				break;
			default :
				break;
		}
		
		if(nextScene != ""){
			SceneManager.LoadSceneAsync(nextScene);
		}
		
    }

    // Update is called once per frame
    void Update()
    {
        
    }
	
	//Debugging
	public void TfgDebug(string txt){
		Debug.Log(txt);
		debuginfo.GetComponent<Text>().text = txt;
	}
	
	//Showing leaderboard
	public void ShowLeaderboard(bool twoButtons){
		leaderboard.SetActive(true);
		if(twoButtons){
			buttonSet0.SetActive(false);
			buttonSet1.SetActive(true);
		}else{
			buttonSet0.SetActive(true);
			buttonSet1.SetActive(false);
		}
		StartCoroutine(getLeaderboardData(gameId, currentUserId));
	}
	
	//Hiding leaderboard
	public void HideLeaderboard(){
		leaderboard.SetActive(false);
		foreach(Transform child in leaderboardContent.transform) {
			GameObject.Destroy(child.gameObject);
		}
        if (nextScene != "")
        {
            SceneManager.LoadSceneAsync(nextScene);
        }
    }
	
	//Re-set the User ID
	public void reSetUserId(string newUserId){
		if(!externallyInjected){
			currentUserId = newUserId;
			StartCoroutine(initialCheckUp(gameId, newUserId));
			TfgDebug("Assigning external User ID " + newUserId);
			externallyInjected = true;
		}else{
			TfgDebug("Already assigned = " + newUserId);
		}
	}
	
	
	//Submitting new earned point
	public void GotScore(int score){
		if(currentUserId != "" && currentUserName != ""){
			if(score <= userHighScore){
				TfgDebug("Gagal: Poin highscore lebih banyak dari poin yang baru didapat.");
			}else{
				userHighScore = score;
				TfgDebug("User " + currentUserId + " mendapatkan poin " + score + "\nSedang submit ke server...");
				StartCoroutine(submitUserScore(gameId, currentUserId, score));
			}
		}else{
			TfgDebug("User ID dan/atau Game ID tidak benar. Cek ulang.");
		}
	}
	
	//Initial game id and user id check
	IEnumerator initialCheckUp(int gid, string uid){
		TfgDebug("Using uid: "+uid+". Sending request to server...");
		WWWForm form = new WWWForm();
        form.AddField("gameid", gid);
        form.AddField("uid", uid);
        using (UnityWebRequest www = UnityWebRequest.Post(backend, form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();
 
            if (www.isNetworkError)
            {
				TfgDebug(www.error);
                Debug.Log(www.error);
            }
            else
            {
                string responseText = clearString(www.downloadHandler.text);;
				TfgDebug("Raw Response: " + responseText);
                if(responseText == "0"){
					TfgDebug("ID Game bermasalah!");
					gameId = 0;
				}
				else if(responseText == "1"){
					TfgDebug("User ID tidak ditemukan.");
					currentUserId = "";
				}else{
					TfgDebug("Response is not 0 or 1");
					string[] responseData = responseText.Split('@');
					TfgDebug("Getting value of responseData[0] = " + responseData[0]);
					if(responseData[0] == "2"){
						TfgDebug("Respon Server OK\nNama Game = " + responseData[1] + "\nNama Pemain = " + responseData[2] + "\nUser ID = " + responseData[4]);
						currentUserId = responseData[4];
						currentUserName = responseData[2];
						userHighScore = int.Parse(responseData[3]);
						OnInitialize?.Invoke("ok");
					}
				}
            }
        }
	}
	
	public int GetHighScore(){
		return userHighScore;
	}
	
	//Submit user's score
	IEnumerator submitUserScore(int gid, string uid, int score){
		WWWForm form = new WWWForm();
        form.AddField("gameid", gid);
        form.AddField("uid", uid);
        form.AddField("score", score);
        using (UnityWebRequest www = UnityWebRequest.Post(backend, form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();
			
			TfgDebug("Contacting " + backend);
 
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = clearString(www.downloadHandler.text);
                if(responseText == "0"){
					TfgDebug("Poin gagal ditambahkan!");
					OnError?.Invoke("Poin gagal ditambahkan!");
				}
				else if(responseText == "1"){
					TfgDebug(score + " poin berhasil ditambahkan kepada " + currentUserName);
					OnSuccess?.Invoke(score + " poin berhasil ditambahkan kepada " + currentUserName);
				}
            }
        }
	}
	
	//Initial game id and user id check
	IEnumerator getLeaderboardData(int gid, string uid){
		WWWForm form = new WWWForm();
        form.AddField("lgameid", gid);
        form.AddField("luid", uid);
        using (UnityWebRequest www = UnityWebRequest.Post(backend, form))
        {
            www.downloadHandler = new DownloadHandlerBuffer();
            yield return www.SendWebRequest();
 
            if (www.isNetworkError)
            {
                Debug.Log(www.error);
            }
            else
            {
                string responseText = clearString(www.downloadHandler.text);
                if(responseText == "0"){
					TfgDebug("Leaderboard: Tidak ada data.");
				}
				else{
					TfgDebug(responseText);
					string[] responseData = responseText.Split('@');
					for(int i = 0; i < responseData.Length; i++){
						if(responseData[i] != ""){
							string[] leaderboardItemData = responseData[i].Split('!');
							GameObject go = Instantiate(leaderboardItem, new Vector3 (0,0,0), Quaternion.identity) as GameObject; 
							go.transform.SetParent(leaderboardContent.transform, false);
							go.GetComponent<LdbItem2>().SetValue(leaderboardItemData[0], leaderboardItemData[1], leaderboardItemData[2], leaderboardItemData[3]);
						}
					}
					scrollView.verticalNormalizedPosition = 1;
				}
            }
        }
	}
	
	public void backToTour(){
		string backUrl = baseurl + "game/?id=" + gameId;
		Application.OpenURL(backUrl);
	}
	
	public string clearString(string txt){
		return txt.Replace("\r", "").Replace("\n", "");
	}
	
}