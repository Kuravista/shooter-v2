﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LdbItem2 : MonoBehaviour
{
    public Text rank;
	public Text name;
	public Text point;
	public Text userid;
	
	public Sprite myItemBg1;
	public Sprite myItemBg2;
	public Sprite myItemBg3;
	public Sprite myItemBg4;
	public Sprite myItemBg5;
	
    public void SetValue(string r, string n, string p, string u){
		rank.text = r;
		name.text = n;
		point.text = p;
		userid.text = u;
		
		if(u == TFG.tampark.currentUserId){
			Debug.Log("Woi, iki akuu!");
			itsMe();
		}
	}
	
	public void itsMe(){
		switch(TFG.tampark.gameId){
			case 1 :
				GetComponent<Image>().sprite = myItemBg1;
				break;
			case 2 :
				GetComponent<Image>().sprite = myItemBg2;
				break;
			case 3 :
				GetComponent<Image>().sprite = myItemBg3;
				break;
			case 4 :
				GetComponent<Image>().sprite = myItemBg4;
				break;
			case 5 :
				GetComponent<Image>().sprite = myItemBg5;
				break;
			default :
				break;
		}
		
	}
}
